package org.nukic.mapwalker.reader;

import org.nukic.mapwalker.infra.Utils;
import org.nukic.mapwalker.map.AsciiMap;

/**
 * Class that reads a map from predefined resource files.
 */
public class ResourceFileMapReader implements MapReader {

    private String path;

    public ResourceFileMapReader(String resourcePath) {
        this.path = resourcePath;
    }

    @Override
    public AsciiMap read() {
        String fileContent = Utils.readResourceRelativePathFile(this.path);
        return new StringMapReader(fileContent).read();
    }
}
