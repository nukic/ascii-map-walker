package org.nukic.mapwalker.reader;

import org.nukic.mapwalker.map.AsciiMap;

public interface MapReader {

    AsciiMap read();
}
