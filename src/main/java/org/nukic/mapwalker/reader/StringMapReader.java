package org.nukic.mapwalker.reader;

import org.nukic.mapwalker.map.AsciiMap;
import org.nukic.mapwalker.infra.Utils;

/**
 * Class that reads a map from String content. Used internally by other map readers and in tests.
 */
public class StringMapReader implements MapReader {

    private String content;

    public StringMapReader(String content) {
        this.content = content;
    }

    @Override
    public AsciiMap read() {
        return new AsciiMap(Utils.toCharMatrix(content));
    }

}
