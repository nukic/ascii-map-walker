package org.nukic.mapwalker.reader;

import org.nukic.mapwalker.infra.Utils;
import org.nukic.mapwalker.map.AsciiMap;

/**
 * Class that reads a map from an absolute path to a file. Called when user provides absolute path as input in
 * application.
 */
public class AbsolutePathMapReader implements MapReader {

    private String path;

    public AbsolutePathMapReader(String path) {
        this.path = path;
    }

    @Override
    public AsciiMap read() {
        String fileContent = Utils.readAbsolutePathFile(this.path);
        return new StringMapReader(fileContent).read();
    }
}
