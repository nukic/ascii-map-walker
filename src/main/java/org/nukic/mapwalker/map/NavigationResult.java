package org.nukic.mapwalker.map;

import static java.util.Objects.nonNull;

public class NavigationResult {

    private Direction direction;
    private String error;

    public NavigationResult(Direction direction) {
        this.direction = direction;
    }

    public NavigationResult(String error) {
        this.error = error;
    }

    public Direction getDirection() {
        return direction;
    }

    public String getError() {
        return error;
    }

    public boolean isValid() {
        return nonNull(this.direction);
    }
}
