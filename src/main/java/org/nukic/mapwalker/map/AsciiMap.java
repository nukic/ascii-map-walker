package org.nukic.mapwalker.map;

import static org.nukic.mapwalker.infra.LogMessages.NOT_ON_A_MAP;
import static org.nukic.mapwalker.infra.LogMessages.NO_START_ERROR;
import static org.nukic.mapwalker.map.Direction.findStepDirection;
import static org.nukic.mapwalker.map.Direction.values;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.nukic.mapwalker.infra.MapWalkerException;
import org.nukic.mapwalker.infra.Utils;

/**
 * An instance of this class represents a map. A map is read by an instance of {@code MapReader}, checked by an
 * instance of {@code MapChecker} and walked through by an instance of read by an instance of {@code MapWalker}.
 */
public class AsciiMap {

    private char[][] map;
    private int traversablePositionCount;

    public AsciiMap(char[][] map) {
        this.map = map;
        this.traversablePositionCount = countTraversableLocations();
    }

    /**
     * Finds first start position on the map. Other start positions (if any) is ignored.
     *
     * @return First start found when iterating through map
     * @throws MapWalkerException if no start position could be found
     */
    public MapLocation findStart() {
        var starts = findLocationsWithChar(Characters.PATH_START);
        if (starts.size() >= 1) {
            return starts.get(0);
        } else /* if (starts.size() == 0) */ {
            throw new MapWalkerException(NO_START_ERROR);
        }
    }

    /**
     * Return all map locations that have searchChar as their character.
     *
     * @param searchChar Character used as condition for map location search
     * @return A list of map location with given character.
     */
    public List<MapLocation> findLocationsWithChar(char searchChar) {
        return findMapLocations(c -> c.equals(searchChar));
    }

    /**
     * Valid map consists of whitespaces and traversable map locations. This method returns all traversable locations
     * on the map.
     *
     * @return A list of all locations that can be traversed, i.e. part of a path.
     */
    public List<MapLocation> findTraversableLocations() {
        return findMapLocations(c -> Characters.isValidPathCharacter(c));
    }

    /**
     * Finds all invalid location (holding invalid map characters). Used by {@code CharacterChecker}.
     *
     * @return List of map locations with invalid charcters.
     */
    public List<MapLocation> findInvalidLocations() {
        return findMapLocations(c -> !Characters.isValidMapCharacter(c));
    }

    /**
     * Counts all traversable locations. Used when deciding if unexpected path end is dead end or broken path.
     *
     * @return Number of all traversable locations in the map
     */
    public int countTraversableLocations() {
        return findTraversableLocations().size();
    }

    /**
     * Higher order function that finds all locations on the map satisfying given predicate. Used mostly internally
     * within this class.
     *
     * @param predicate A predicate that should be satisfied in locations we are looking for
     * @return A list of map locations that satisfy predicate.
     */
    public List<MapLocation> findMapLocations(Predicate<Character> predicate) {
        List<MapLocation> locations = new ArrayList<>();
        for (int i = 0; i < this.map.length; i++) {
            for (int j = 0; j < this.map[i].length; j++) {
                if (predicate.test(map[i][j])) {
                    locations.add(new MapLocation(i, j, map[i][j]));
                }
            }
        }
        return locations;
    }

    /**
     * Gets a map location using row/column coordinates.
     *
     * @param row    Row index, staring from 0
     * @param column Column index, staring from 0
     * @return Map location at given row/column coordinates
     * @throws MapWalkerException with "NOT_ON_A_MAP" reason if no such location exist
     */
    public MapLocation getMapLocation(int row, int column) {
        if (!isOnMap(row, column)) {
            throw new MapWalkerException(String.format(NOT_ON_A_MAP, row, column, this));
        }
        return new MapLocation(row, column, map[row][column]);
    }

    /**
     * Makes a step given current position and direction.
     *
     * @param direction Direction of the next step
     * @param current   Current map location
     * @return New map location or null, if step cannot be made
     */
    public MapLocation makeStep(Direction direction, MapLocation current) {
        if (canMakeStep(direction, current)) {
            return getMapLocation(current.getRow() + direction.getRowDelta(),
                                  current.getColumn() + direction.getColumnDelta());
        } else {
            return null;
        }
    }

    /**
     * We can keep direction if we can make step in the same direction as previous step.
     *
     * @param previous Previous map location
     * @param current  Current map location
     * @return True if we can make step in same direction, false otherwise
     */
    public boolean canKeepDirection(MapLocation previous, MapLocation current) {
        var previousDirection = findStepDirection(previous, current);
        return canMakeStep(previousDirection, current);
    }

    /**
     * Checks if a step can be made, given current location and direction. Basically checks if next location is on
     * the map and is it traversable.
     *
     * @param direction A direction of next step
     * @param current   Current map location
     * @return True if step can be made, false otherwise.
     */
    public boolean canMakeStep(Direction direction, MapLocation current) {
        var row = current.getRow() + direction.getRowDelta();
        var column = current.getColumn() + direction.getColumnDelta();
        return isOnMap(row, column) ? isTraversable(row, column) : false;
    }

    /**
     * Obtains all possible direction in which a step can be made, ignoring the previous step location. This implies
     * that this will return also direction to go back a step.
     *
     * @param current Current map location
     * @return A set of directions in which a step can be made from current location.
     */
    public Set<Direction> getPossibleDirections(MapLocation current) {
        return Arrays.stream(values()).filter(d -> canMakeStep(d, current)).collect(Collectors.toSet());
    }

    /**
     * Checks if map location with givebn row/column coordinates can be traversed (part of a path)
     *
     * @param row    Row index, can be 0
     * @param column Column index, can be 0
     * @return True if map location at given coordinated can be traversed (part of a path)
     */
    public boolean isTraversable(int row, int column) {
        var c = map[row][column];
        return Characters.isValidPathCharacter(c);
    }

    @Override
    public String toString() {
        return Utils.toString(map);
    }

    private boolean isOnMap(int row, int column) {
        return mapHasRow(row) && rowHasColumn(row, column);
    }

    private boolean rowHasColumn(int row, int column) {
        return column < map[row].length && column >= 0;
    }

    private boolean mapHasRow(int row) {
        return row < map.length && row >= 0;
    }
}
