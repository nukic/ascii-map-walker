package org.nukic.mapwalker.map;

import java.util.Set;
import org.nukic.mapwalker.infra.MapWalkerException;
import org.nukic.mapwalker.infra.LogMessages;

/**
 * Enumeration specifying allowed directions. Diagonal steps are not allowed.
 */
public enum Direction {
    RIGHT(0, 1), LEFT(0, -1), UP(-1, 0), DOWN(1, 0);

    int rowDelta;
    int columnDelta;

    Direction(int rowDelta, int columnDelta) {
        this.rowDelta = rowDelta;
        this.columnDelta = columnDelta;
    }

    public int getRowDelta() {
        return rowDelta;
    }

    public int getColumnDelta() {
        return columnDelta;
    }

    /**
     * Gets the opposite direction of the one that is provided. Since one of the deltas is always 0, we are basically
     * changing sign of only one delta here. Since we do not know which one needs to be changed, we change both
     * knowing that only one will be actually applied.
     *
     * @param direction Direction for which we are looking an opposite
     * @return Opposite direction
     */
    public static Direction getOpposite(Direction direction) {
        return fromDeltas(-direction.getRowDelta(), -direction.getColumnDelta());
    }

    /**
     * Gets all directions if we decide to turn from current direction.
     *
     * @param direction Current direction, the one we are looking turn directions
     * @return A set of turn directions of the current one
     */
    public static Set<Direction> getAllTurnDirections(Direction direction) {
        var rowColumnSwitched = fromDeltas(direction.getColumnDelta(), direction.getRowDelta());
        var opposite = getOpposite(rowColumnSwitched);
        return Set.of(rowColumnSwitched, opposite);
    }

    /**
     * Creates a direction given the row and column delta. Delta values are restricted, so there isa lot of space for
     * an error. Throws exception if error happens.
     * Row and column delta can only have 0, 1, and -1 values. Row and columns cannot, at the same time have absolute
     * value of 1, because it would imply diagonal direction.
     * If row delta is 0, we are moving within one row, so possible directions are only LEFT (column delta = -1) or
     * RIGHT (column delta = 1).
     * If column delta is 0, we are moving in one single column, so possible directions are only UP (row delta = -1)
     * or DOWN (row delta = 1)
     *
     * @param rowDelta    Number of rows to change, can only be 0, 1 or -1
     * @param columnDelta Number of columns to change, can only be 0, 1 or -1
     * @return Direction created from those deltas, or an exception if invalid row/column delta is provided.
     */
    public static Direction fromDeltas(int rowDelta, int columnDelta) {
        if (Math.abs(rowDelta) == Math.abs(columnDelta)) {
            throw new MapWalkerException(LogMessages.DELTAS_MUST_NOT_BE_SAME);
        }
        if (Math.abs(rowDelta) > 1 || Math.abs(columnDelta) > 1) {
            throw new MapWalkerException(LogMessages.DELTAS_MUST_NOT_BE_LARGER_THAN_1);
        }
        if (rowDelta == 0) {
            return columnDelta == 1 ? RIGHT : LEFT;
        } else /*if (columnDelta == 0)*/ {
            return rowDelta == 1 ? DOWN : UP;
        }
    }

    /**
     * Detects step direction when two subsequent locations are provided.
     *
     * @param previous Previous step location
     * @param current  Current step location
     * @return Direction of a step
     */
    public static Direction findStepDirection(MapLocation previous, MapLocation current) {
        var rowDelta = current.getRow() - previous.getRow();
        var columnDelta = current.getColumn() - previous.getColumn();
        return fromDeltas(rowDelta, columnDelta);
    }
}
