package org.nukic.mapwalker.map;

import static org.nukic.mapwalker.infra.Utils.containedIn;

public class Characters {

    public static final char PATH_START = '@';
    public static final char PATH_END = 'x';
    public static final char PATH_TURN = '+';
    public static final char WHITESPACE = ' ';
    public static final Character[] VALID_PATH_CHARACTERS =
        { '@', '+', '-', '|', 'A', 'B', 'C', 'D', 'E', 'G', 'H', 'I', 'L', 'N', 'O', 'S', 'x' };

    private Characters() {
    }

    /**
     * Checks if character can be part of a PATH
     *
     * @param c Character to be checked
     * @return True if given character can be part of a PATH
     */
    public static boolean isValidPathCharacter(char c) {
        return containedIn(VALID_PATH_CHARACTERS, c);
    }

    /**
     * Checks if character can be part of a MAP
     *
     * @param c Character to be checked
     * @return True if given character can be part of a MAP
     */
    public static boolean isValidMapCharacter(Character c) {
        return containedIn(VALID_PATH_CHARACTERS, c) || c.equals(WHITESPACE);
    }
}
