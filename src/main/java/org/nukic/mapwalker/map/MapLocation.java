package org.nukic.mapwalker.map;

import static java.util.Objects.nonNull;
import static org.nukic.mapwalker.infra.LogMessages.LOG_LOCATION;
import static org.nukic.mapwalker.infra.LogMessages.LOG_LOCATION_WITH_ERROR;


import java.util.Objects;

/**
 * Single map location. Has row/column coordinates, assigned character and optionally an error which has been
 * localized to this location.
 */
public class MapLocation {

    private final int row;
    private final int column;
    private final char character;
    private String error;

    public MapLocation(int row, int column, char character) {
        this.row = row;
        this.column = column;
        this.character = character;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public char getCharacter() {
        return character;
    }

    public boolean is(char c) {
        return this.character == c;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MapLocation that = (MapLocation) o;
        return row == that.row && column == that.column && character == that.character;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, column, character);
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return this.error;
    }

    public boolean hasError() {
        return nonNull(this.error);
    }

    @Override
    public String toString() {
        if (this.hasError()) {
            return String.format(LOG_LOCATION_WITH_ERROR, this.character, this.row, this.column, this.error);
        } else {
            return String.format(LOG_LOCATION, this.character, this.row, this.column);
        }
    }
}
