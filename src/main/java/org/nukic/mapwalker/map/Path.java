package org.nukic.mapwalker.map;

import static java.lang.Character.isUpperCase;
import static java.util.Objects.isNull;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A path traversed by the walk. Keeps track of all visited locations and collected letters.
 */
public class Path {

    private List<Character> letters;
    private List<MapLocation> locations;

    public Path() {
        this.letters = new ArrayList<>();
        this.locations = new ArrayList<>();
    }

    /**
     * Adds a location to this path. This means that it updates traversed locations ({@code path}
     * locations). If location character is uppercase letter and has not been visited yet (not in list of visited
     * locations), it also updates ({@code letters} attribute).
     *
     * @param location A map location to be added to this path.
     */
    public void save(MapLocation location) {
        if (isNull(location)) {
            return;
        }
        var charAtLocation = location.getCharacter();
        if (isUpperCase(charAtLocation) && !locations.contains(location)) {
            letters.add(charAtLocation);
        }
        locations.add(location);
    }

    public String getPathAsString() {
        return locations.stream().map(l -> String.valueOf(l.getCharacter())).collect(Collectors.joining());
    }

    public long count(char searchChar) {
        return this.locations.stream().filter(l -> l.is(searchChar)).count();
    }

    public List<MapLocation> getLocations() {
        return locations;
    }

    /**
     * Finds first location on this path with given character and returns it.
     *
     * @param searchChar Character for which we are looking first map location
     * @return First map location with given map loocation.
     */
    public MapLocation getFirstLocationWithChar(char searchChar) {
        return this.locations.stream().filter(l -> l.is(searchChar)).findFirst().orElse(null);
    }

    /**
     * Returns a list of all map locations found on this map found after given one. Given location is included as the
     * first element in this list.
     *
     * @param location A location after which we want all locations
     * @return A list of locations after the given location
     */
    public List<MapLocation> getAllAfter(MapLocation location) {
        return this.locations.stream().dropWhile(l -> !l.equals(location)).collect(Collectors.toList());
    }

    /**
     * Returns a list of all map locations found on this map found before given one. Given location is NOT included
     * in the list.
     *
     * @param location A location before which we want all locations
     * @return A list of path locations before the given location
     */
    public List<MapLocation> getAllBefore(MapLocation location) {
        return this.locations.stream().takeWhile(l -> !l.equals(location)).collect(Collectors.toList());
    }

    public String getCollectedLettersAsString() {
        return letters.stream().map(String::valueOf).collect(Collectors.joining());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Path that = (Path) o;
        return Objects.equals(letters, that.letters) && Objects.equals(locations, that.locations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(letters, locations);
    }
}
