package org.nukic.mapwalker;

import static java.util.Objects.nonNull;
import static org.nukic.mapwalker.infra.LogMessages.COLLECTED_LETTERS;
import static org.nukic.mapwalker.infra.LogMessages.ERROR_READING_PATH;
import static org.nukic.mapwalker.infra.LogMessages.INVALID_MAP_ERROR;
import static org.nukic.mapwalker.infra.LogMessages.LOADING_ABSOLUTE_PATH_MAP;
import static org.nukic.mapwalker.infra.LogMessages.LOADING_RESOURCE_MAP;
import static org.nukic.mapwalker.infra.LogMessages.NO_RESOURCE_MAP;
import static org.nukic.mapwalker.infra.LogMessages.RUNNING;
import static org.nukic.mapwalker.infra.LogMessages.TRAVERSED_PATH;
import static org.nukic.mapwalker.infra.LogMessages.USAGE;


import org.nukic.mapwalker.checker.MainChecker;
import org.nukic.mapwalker.infra.ResourceMaps;
import org.nukic.mapwalker.map.AsciiMap;
import org.nukic.mapwalker.reader.ResourceFileMapReader;
import org.nukic.mapwalker.walker.SunshineWalker;
import org.nukic.mapwalker.reader.AbsolutePathMapReader;

public class App {
    public static void main(String[] args) {
        System.out.println(RUNNING);
        if (args.length != 1) {
            printHelp();
        }

        Integer mapNumber = null;
        String absolutePath = null;
        var arg = args[0];
        try {
            mapNumber = Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            absolutePath = arg;
        }

        AsciiMap map = loadMap(mapNumber, absolutePath);
        if (nonNull(map)) {
            validateAndWalk(map);
        }
    }

    private static void validateAndWalk(AsciiMap map) {
        System.out.println(map.toString());
        var checkResult = new MainChecker().check(map);
        if (checkResult.isValid()) {
            var path = new SunshineWalker().walk(map);
            System.out.println(String.format(TRAVERSED_PATH, path.getPathAsString()));
            System.out.println(String.format(COLLECTED_LETTERS, path.getCollectedLettersAsString()));
        } else {
            System.err.println(String.format(INVALID_MAP_ERROR, checkResult.getErrorMessage()));
        }
    }

    private static AsciiMap loadMap(Integer mapNumber, String absolutePath) {
        AsciiMap map = null;
        if (nonNull(absolutePath)) {
            map = readMapFromAbsolutePath(absolutePath);
        } else if (nonNull(mapNumber)) {
            map = readMapFromResource(mapNumber);
        }
        return map;
    }

    private static AsciiMap readMapFromResource(Integer mapNumber) {
        if (mapNumber >= 1 && mapNumber <= 23) {
            System.out.println(String.format(LOADING_RESOURCE_MAP, mapNumber));
            return new ResourceFileMapReader(ResourceMaps.get(mapNumber)).read();
        } else {
            System.err.println(String.format(NO_RESOURCE_MAP, mapNumber));
            return null;
        }
    }

    private static AsciiMap readMapFromAbsolutePath(String absolutePath) {
        try {
            System.out.println(String.format(LOADING_ABSOLUTE_PATH_MAP, absolutePath));
            return new AbsolutePathMapReader(absolutePath).read();
        } catch (Exception e) {
            System.err.println(String.format(ERROR_READING_PATH, absolutePath, e.getMessage()));
            return null;
        }
    }

    private static void printHelp() {
        System.out.println(USAGE);
    }
}
