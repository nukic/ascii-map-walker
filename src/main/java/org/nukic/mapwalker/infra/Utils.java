package org.nukic.mapwalker.infra;

import static java.util.Objects.isNull;
import static org.nukic.mapwalker.infra.LogMessages.CANNOT_READ_PATH;
import static org.nukic.mapwalker.infra.LogMessages.ERROR_CLOSING_INPUT_STREAM;
import static org.nukic.mapwalker.infra.LogMessages.INVALID_DIR_PATH;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {

    public static final String NEWLINE = System.getProperty("line.separator");

    private Utils() {
    }

    /**
     * Converts a string to char matrix. First it chops the string by newlines and the does the rest.
     *
     * @param str Input string to be converted
     * @return A character matrix (2D array) obtained from input string
     */
    public static char[][] toCharMatrix(String str) {
        var linesAsCharArray = Arrays.stream(str.split(NEWLINE)).map(l -> l.toCharArray()).collect(Collectors.toList());
        char[][] chars = new char[linesAsCharArray.size()][];
        return linesAsCharArray.toArray(chars);
    }

    /**
     * Converts the char matrix (2D array) int a multi-line string. Each row of matrix is single line in a string.
     *
     * @param charMatrix Input matrix
     * @return String obtained by converting matrix rows into string lines
     */
    public static String toString(char[][] charMatrix) {
        StringBuffer sb = new StringBuffer("");
        for (int i = 0; i < charMatrix.length; i++) {
            sb.append(new String(charMatrix[i]));
            sb.append(NEWLINE);
        }
        return sb.toString();
    }

    public static boolean containedIn(Character[] chars, char ch) {
        return Arrays.stream(chars).anyMatch(c -> c == ch);
    }

    /**
     * Reads content of a file given by a resource-relative path.
     *
     * @param resourceRelativePath A resource-relative path to a file
     * @return Content of a file, as a string
     */
    public static String readResourceRelativePathFile(String resourceRelativePath) {
        InputStream inputStream = null;
        try {
            inputStream = Utils.class.getClassLoader().getResourceAsStream(resourceRelativePath);
            if (isNull(inputStream)) {
                throw new MapWalkerException(String.format(CANNOT_READ_PATH, resourceRelativePath));
            }
            return readFromInputStream(inputStream);
        } catch (IOException e) {
            throw new MapWalkerException(String.format(CANNOT_READ_PATH, resourceRelativePath), e);
        } finally {
            closeInputStream(resourceRelativePath, inputStream);
        }
    }

    private static String readFromInputStream(InputStream is) throws IOException {
        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
                contentBuilder.append(line).append("\n");
            }
        }
        return contentBuilder.toString();
    }

    private static void closeInputStream(String resourceRelativePath, InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                System.out.println(String.format(ERROR_CLOSING_INPUT_STREAM, resourceRelativePath));
            }
        }
    }

    /**
     * Reads content of a file given by an absolute path.
     *
     * @param absolutePath An absolute path to a file
     * @return Content of a file, as a string
     */
    public static String readAbsolutePathFile(String absolutePath) {
        return readFile(Paths.get(absolutePath));
    }

    public static String readFile(Path path) {
        try {
            return new String(Files.readAllBytes(path));
        } catch (IOException e) {
            throw new MapWalkerException(String.format(CANNOT_READ_PATH, path.toString()), e);
        }
    }

    /**
     * Obtains a list of paths contained in directory given by resource-relative input path.
     *
     * @param folder A resource-relative directory path
     * @return List of (file) paths located in given dir
     */
    public static List<String> getResourceDirFilePaths(String folder) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL url = loader.getResource(folder);
        if (isNull(url)) {
            throw new MapWalkerException(String.format(CANNOT_READ_PATH, folder));
        }
        var dir = new File(url.getPath());
        if (!dir.isDirectory()) {
            throw new MapWalkerException(String.format(INVALID_DIR_PATH, folder));
        }
        return Arrays.stream(dir.listFiles()).map(f -> f.getPath()).collect(Collectors.toList());
    }
}
