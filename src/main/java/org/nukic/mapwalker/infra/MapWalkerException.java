package org.nukic.mapwalker.infra;

public class MapWalkerException extends RuntimeException {

    public MapWalkerException(String message) {
        super(message);
    }

    public MapWalkerException(String message, Throwable t) {
        super(message, t);
    }
}
