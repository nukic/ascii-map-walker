package org.nukic.mapwalker.infra;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ResourceMaps {

    public static final String MAP1 = "maps/valid/map1.txt";
    public static final String MAP2 = "maps/valid/map2.txt";
    public static final String MAP3 = "maps/valid/map3.txt";
    public static final String MAP4 = "maps/valid/map4.txt";
    public static final String MAP5 = "maps/valid/map5.txt";
    public static final String MAP6 = "maps/invalid/map6.txt";
    public static final String MAP7 = "maps/invalid/map7.txt";
    public static final String MAP8 = "maps/invalid/map8.txt";
    public static final String MAP9 = "maps/invalid/map9.txt";
    public static final String MAP10 = "maps/invalid/map10.txt";
    public static final String MAP11 = "maps/invalid/map11.txt";
    public static final String MAP12 = "maps/invalid/map12.txt";
    public static final String MAP13 = "maps/invalid/map13.txt";
    public static final String MAP14 = "maps/custom/map14.txt";
    public static final String MAP15 = "maps/custom/map15.txt";
    public static final String MAP16 = "maps/custom/map16.txt";
    public static final String MAP17 = "maps/custom/map17.txt";
    public static final String MAP18 = "maps/custom/map18.txt";
    public static final String MAP19 = "maps/custom/map19.txt";
    public static final String MAP20 = "maps/custom/map20.txt";
    public static final String MAP21 = "maps/custom/map21.txt";
    public static final String MAP22 = "maps/custom/map22.txt";
    public static final String MAP23 = "maps/custom/map23.txt";

    private ResourceMaps() {
    }

    private static Map<Integer, String> mapPaths;

    static {
        mapPaths = new HashMap<Integer, String>();
        mapPaths.put(1, MAP1);
        mapPaths.put(2, MAP2);
        mapPaths.put(3, MAP3);
        mapPaths.put(4, MAP4);
        mapPaths.put(5, MAP5);
        mapPaths.put(6, MAP6);
        mapPaths.put(7, MAP7);
        mapPaths.put(8, MAP8);
        mapPaths.put(9, MAP9);
        mapPaths.put(10, MAP10);
        mapPaths.put(11, MAP11);
        mapPaths.put(12, MAP12);
        mapPaths.put(13, MAP13);
        mapPaths.put(14, MAP14);
        mapPaths.put(15, MAP15);
        mapPaths.put(16, MAP16);
        mapPaths.put(17, MAP17);
        mapPaths.put(18, MAP18);
        mapPaths.put(19, MAP19);
        mapPaths.put(20, MAP20);
        mapPaths.put(21, MAP21);
        mapPaths.put(22, MAP22);
        mapPaths.put(23, MAP23);
    }

    public static String get(int mapNumber) {
        return mapPaths.get(mapNumber);
    }

    public static Collection<String> getAll() {
        return mapPaths.values();
    }

}
