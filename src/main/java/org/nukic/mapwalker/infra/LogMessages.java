package org.nukic.mapwalker.infra;

public class LogMessages {

    private LogMessages() {
    }

    public static final String LOG_LOCATION = "Char '%c' at (%d, %d)";
    public static final String LOG_LOCATION_WITH_ERROR = "Char '%c' at (%d, %d) has error: '%s'";
    public static final String NOT_ON_A_MAP = "Location (%d, %d) is not on a map: \n%s";
    public static final String DELTAS_MUST_NOT_BE_LARGER_THAN_1 =
        "Row and column differences must not be larger than 1";
    public static final String DELTAS_MUST_NOT_BE_SAME = "Row and column delta must not have same absolute value";
    public static final String T_FORK = "T-fork found";
    public static final String FAKE_TURN = "Fake turn found";
    public static final String UNKNOWN_ERROR = "Unknown error";
    public static final String BROKEN_PATH = "Broken path found";
    public static final String MULTIPLE_STARTING_PATHS = "Multiple starting paths found.";
    public static final String NO_STARTING_PATHS = "No starting paths found.";
    public static final String CANNOT_READ_PATH = "Could not read path '%s'";
    public static final String CANNOT_PARSE_URI = "Could not parse URI '%s'";
    public static final String INVALID_DIR_PATH = "Path '%s' does not exist or is not a dir";
    public static final String MULTIPLE_STARTS_ERROR = "Multiple starts found!";
    public static final String MULTIPLE_ENDS_ERROR = "Multiple ends found!";
    public static final String NO_START_ERROR = "No start found!";
    public static final String NO_END = "No end found";
    public static final String INVALID_CHARACTERS = "Invalid characters found!";
    public static final String NO_RESOURCE_MAP = "No resource map with number %d can be found.";
    public static final String ERROR_READING_PATH = "An error happened while reading path '%s': '%s'";
    public static final String TRAVERSED_PATH = "Traversed path: \t%s";
    public static final String COLLECTED_LETTERS = "Collected letters: \t%s";
    public static final String INVALID_MAP_ERROR = "Cannot walk through an invalid map. Error: '%s'";
    public static final String LOADING_RESOURCE_MAP = "Loading resource map: %d";
    public static final String LOADING_ABSOLUTE_PATH_MAP = "Loading map from absolute path: %s";
    public static final String ERROR_CLOSING_INPUT_STREAM = "Error closing input stream on file %s";
    public static final String RUNNING = "Running AsciiMapWalker...";
    public static final String USAGE = "USAGE: \n" + "Only one argument allowed.\n" +
                                       "If a number, values 1-23 are allowed (indicating predefined map to walk).\n" +
                                       "If a string, an absolute path to file with map should be provided.";
}
