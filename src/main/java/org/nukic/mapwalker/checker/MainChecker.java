package org.nukic.mapwalker.checker;

import java.util.ArrayList;
import java.util.List;
import org.nukic.mapwalker.map.AsciiMap;

/**
 * Main checker that only invoke other, sub-checkers. Sub-checkers can be static, which analyse map as a whole
 * ignoring the fact that there might be parts of map that are unreachable. Walking checker is the opposite: it is
 * able to detect errors on the traversed path only. Together, those two types of checkers make a good base for any
 * type of check that user may want for a map.
 * <p>
 * If any of sub-checkers fails, main checker also fails. Not only that, since errors may be (and usually are)
 * interrelated, checking process is only interested in first found error. This means that order of checkers is
 * important.
 */
public class MainChecker implements MapChecker {

    List<MapChecker> checkers;

    public MainChecker() {
        this.checkers = new ArrayList<>();
        //Some other static checker?
        this.checkers.add(new CharacterChecker());
        this.checkers.add(new StartChecker());
        this.checkers.add(new EndChecker());
        this.checkers.add(new WalkingChecker());
    }

    /**
     * Checks given map against all known errors. In case check fails, returns first failure result,
     * because errors may be dependent and remaining errors may be misleading.
     *
     * @param map Map to be checked
     * @return Check result object indicating if map is valid, and, in case it is not, an error message of
     * the <b>first<b/> failed checker.
     */
    @Override
    public CheckResult check(AsciiMap map) {
        return checkers.stream().map(c -> c.check(map)).filter(r -> !r.isValid()).findFirst().orElse(new CheckResult());
    }
}
