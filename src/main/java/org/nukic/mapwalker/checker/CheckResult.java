package org.nukic.mapwalker.checker;

public class CheckResult {
    private final boolean isValid;
    private final String errorMessage;

    public CheckResult(String errorMessage) {
        this.isValid = false;
        this.errorMessage = errorMessage;
    }

    public CheckResult() {
        this.isValid = true;
        this.errorMessage = "";
    }

    public boolean isValid() {
        return isValid;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
