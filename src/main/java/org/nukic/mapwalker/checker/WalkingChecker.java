package org.nukic.mapwalker.checker;

import static org.nukic.mapwalker.infra.LogMessages.MULTIPLE_ENDS_ERROR;
import static org.nukic.mapwalker.infra.LogMessages.MULTIPLE_STARTS_ERROR;


import java.util.Objects;
import java.util.stream.Collectors;
import org.nukic.mapwalker.infra.MapWalkerException;
import org.nukic.mapwalker.map.AsciiMap;
import org.nukic.mapwalker.map.MapLocation;
import org.nukic.mapwalker.map.Path;
import org.nukic.mapwalker.map.Characters;
import org.nukic.mapwalker.walker.MapWalker;
import org.nukic.mapwalker.walker.MainWalker;

/**
 * Walking checker is not checking whole map, but tries to walk the map and detect errors as it traverses the map.
 * Since checking process here implies walking the map, valid maps may be walked twice, once for validation and once
 * "for real". Note that nature of walking checker is very different from static checkers that observe complete map.
 * <p>
 * Note that walking checker implements both {@code MapChecker} and {@code MapWalker} interface.
 */
public class WalkingChecker implements MapChecker, MapWalker {

    /**
     * Checks the map by trying to walk through it. Traversed path locations have associated localized errors. So
     * after a successful walk, checking is basically post-processing of the path and its errors; counting relevant
     * positions and similar.
     * Walking checker does not stop on end location, but it tries to continue after it, until it is possible to walk.
     * However, note that most of the errors that happen after first end can safely be ignored, but not all of them.
     *
     * @param map A map to be checked
     * @return Checking result indicating if map is valid, and if it is not, what kind of error is present.
     */
    @Override
    public CheckResult check(AsciiMap map) {
        try {
            var path = walk(map);
            var firstEnd = path.getFirstLocationWithChar(Characters.PATH_END);
            // We are only interested in errors before the first end
            var errorLocations =
                path.getAllBefore(firstEnd).stream().filter(MapLocation::hasError).collect(Collectors.toList());
            if (!errorLocations.isEmpty()) {
                return new CheckResult(errorLocations.get(0).getError());
            }
            if (path.count(Characters.PATH_END) > 1) {
                return new CheckResult(MULTIPLE_ENDS_ERROR);
            }
            if (path.count(Characters.PATH_START) > 1) {
                return new CheckResult(MULTIPLE_STARTS_ERROR);
            }
            return new CheckResult();
        } catch (MapWalkerException e) {
            return new CheckResult(e.getMessage());
        }
    }

    /**
     * Walking checker walks the map until next location is not null, i.e. there are no possible locations to walk to.
     * Reuses the walking logic from {@code MainWalker} by utilizing its higHer-order {@code walkWhile()} method.
     *
     * @param map A map to be walked
     * @return Traversed path
     */
    @Override
    public Path walk(AsciiMap map) {
        return new MainWalker(map).walkWhile(Objects::nonNull);
    }
}
