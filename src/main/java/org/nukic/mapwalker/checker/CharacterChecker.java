package org.nukic.mapwalker.checker;

import static org.nukic.mapwalker.infra.LogMessages.INVALID_CHARACTERS;


import org.nukic.mapwalker.map.AsciiMap;

/**
 * Checks if complete map contains invalid characters. Ignores the fact that some map locations might not even be
 * reachable.
 */
public class CharacterChecker implements MapChecker {

    @Override
    public CheckResult check(AsciiMap map) {
        var hasInvalidChars = map.findInvalidLocations().size() > 0;
        return hasInvalidChars ? new CheckResult(INVALID_CHARACTERS) : new CheckResult();
    }
}
