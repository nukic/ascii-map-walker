package org.nukic.mapwalker.checker;

import static org.nukic.mapwalker.infra.LogMessages.NO_START_ERROR;


import org.nukic.mapwalker.map.AsciiMap;
import org.nukic.mapwalker.map.Characters;

/**
 * Start checker checks if there is a start character in a map. It does not check for multiple starts, because, if
 * there might be starts outside the path. Multiple starts (in the path) are checked by {@code WalkingChecker}.
 */
public class StartChecker implements MapChecker {
    @Override
    public CheckResult check(AsciiMap map) {
        var startCount = map.findLocationsWithChar(Characters.PATH_START).size();
        return startCount == 0 ? new CheckResult(NO_START_ERROR) : new CheckResult();
    }
}
