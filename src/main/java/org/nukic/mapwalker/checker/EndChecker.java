package org.nukic.mapwalker.checker;

import static org.nukic.mapwalker.infra.LogMessages.NO_END;


import org.nukic.mapwalker.map.AsciiMap;
import org.nukic.mapwalker.map.Characters;

/**
 * End checker checks if there is an end in a map. It does not check for multiple ends, because, if there might be
 * end outside the path. Multiple ends (in the path) are checked by {@code WalkingChecker}.
 */
public class EndChecker implements MapChecker {
    @Override
    public CheckResult check(AsciiMap map) {
        var endCount = map.findLocationsWithChar(Characters.PATH_END).size();
        return endCount == 0 ? new CheckResult(NO_END) : new CheckResult();
    }
}
