package org.nukic.mapwalker.checker;

import org.nukic.mapwalker.map.AsciiMap;

public interface MapChecker {

    CheckResult check(AsciiMap map);
}
