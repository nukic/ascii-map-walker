package org.nukic.mapwalker.walker;

import static org.nukic.mapwalker.map.Characters.PATH_END;


import org.nukic.mapwalker.map.AsciiMap;
import org.nukic.mapwalker.map.Path;

/**
 * Sunshine walker is only walking over valid maps. This is ensured by checkers that run before. Sunshine walker walks
 * until the first end character on the path.
 */
public class SunshineWalker implements MapWalker {

    public Path walk(AsciiMap map) {
        return new MainWalker(map).walkWhile(location -> !location.is(PATH_END));
    }
}
