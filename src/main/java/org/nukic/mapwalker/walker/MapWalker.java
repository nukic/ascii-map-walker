package org.nukic.mapwalker.walker;

import org.nukic.mapwalker.map.AsciiMap;
import org.nukic.mapwalker.map.Path;

public interface MapWalker {

    Path walk(AsciiMap map);

}
