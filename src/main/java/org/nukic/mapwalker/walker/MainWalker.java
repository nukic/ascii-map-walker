package org.nukic.mapwalker.walker;

import static java.lang.Character.isUpperCase;
import static java.util.Objects.isNull;
import static org.nukic.mapwalker.infra.LogMessages.BROKEN_PATH;
import static org.nukic.mapwalker.infra.LogMessages.FAKE_TURN;
import static org.nukic.mapwalker.infra.LogMessages.MULTIPLE_STARTING_PATHS;
import static org.nukic.mapwalker.infra.LogMessages.MULTIPLE_STARTS_ERROR;
import static org.nukic.mapwalker.infra.LogMessages.NO_END;
import static org.nukic.mapwalker.infra.LogMessages.NO_STARTING_PATHS;
import static org.nukic.mapwalker.infra.LogMessages.T_FORK;
import static org.nukic.mapwalker.infra.LogMessages.UNKNOWN_ERROR;
import static org.nukic.mapwalker.map.Direction.findStepDirection;
import static org.nukic.mapwalker.map.Direction.getAllTurnDirections;


import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.nukic.mapwalker.map.AsciiMap;
import org.nukic.mapwalker.map.Direction;
import org.nukic.mapwalker.map.MapLocation;
import org.nukic.mapwalker.map.NavigationResult;
import org.nukic.mapwalker.map.Path;
import org.nukic.mapwalker.map.Characters;

/**
 * Main walker holds walking logic reused in {@code WalkingChecker} class and {@code SunshineWalker}.
 * Notice that, although this is main walking method, it does not implement {@code MapWalker} interface.
 */
public class MainWalker {

    private AsciiMap map;
    private Path path;

    public MainWalker(AsciiMap map) {
        this.map = map;
        this.path = new Path();
    }

    /**
     * Main walking logic is implemented here. Called by {@code walk()} method in {@code WalkingChecker} class and
     * {@code SunshineWalker}. Walking checker walks until it can (next position is null), while sunshine walker walks
     * until the first end position.
     *
     * @param condition A condition that must be satisfied in order to keep walking
     * @return A path traversed on the map
     */
    public Path walkWhile(Predicate<MapLocation> condition) {
        MapLocation previousLocation = null;
        var currentLocation = map.findStart();
        while (condition.test(currentLocation)) {
            //System.out.println(currentLocation);
            path.save(currentLocation);
            var temp = currentLocation;
            currentLocation = makeStep(previousLocation, currentLocation);
            previousLocation = temp;
        }
        path.save(currentLocation);
        return path;
    }

    private MapLocation makeStep(MapLocation previous, MapLocation current) {
        // Find direction
        var navigationResult = navigate(previous, current);
        if (!navigationResult.isValid()) {
            current.setError(navigationResult.getError());
            return null;
        }
        // Try to make a step
        var next = map.makeStep(navigationResult.getDirection(), current);
        if (unexpectedEnd(current, next)) {
            current.setError(visitedAllLocations() ? NO_END : BROKEN_PATH);
            return null;
        }
        return next;
    }

    private boolean unexpectedEnd(MapLocation current, MapLocation next) {
        return isNull(next) && !current.is(Characters.PATH_END);
    }

    private NavigationResult navigate(MapLocation previous, MapLocation current) {
        if (isFirstStep(previous)) {
            return navigateFromStart(current);
        } else {
            if (current.is(Characters.PATH_START)) {
                return new NavigationResult(MULTIPLE_STARTS_ERROR);
            }
            if (canMakeTurnAt(current)) {
                return navigateAtTurn(previous, current);
            } else {
                return new NavigationResult(findPreviousStepDirection(previous, current));
            }
        }
    }

    private Direction findPreviousStepDirection(MapLocation previous, MapLocation current) {
        return findStepDirection(previous, current);
    }

    private boolean isFirstStep(MapLocation previous) {
        return isNull(previous);
    }

    private NavigationResult navigateFromStart(MapLocation current) {
        var possibleDirections = map.getPossibleDirections(current);
        if (possibleDirections.size() > 1) {
            return new NavigationResult(MULTIPLE_STARTING_PATHS);
        } else if (possibleDirections.size() == 0) {
            return new NavigationResult(NO_STARTING_PATHS);
        } else /* if (possibleDirections.size() == 1) */ {
            return new NavigationResult(possibleDirections.iterator().next());
        }
    }

    /**
     * Called if we can turn at this map location (i.e. we are at UPPERCASE letter or '+'). If we are at UPPERCASE
     * letter location we can keep direction. However, if we are at TURN location, we must turn.
     *
     * @param previous Previous map location
     * @param current  Current map location
     * @return A list of allowed direction for the next step
     */
    private NavigationResult navigateAtTurn(MapLocation previous, MapLocation current) {
        var previousDirection = findPreviousStepDirection(previous, current);
        if (shouldKeepDirection(previous, current)) {
            return new NavigationResult(previousDirection);
        } else {
            var turnDirections = findTurnDirections(current, previousDirection);
            if (turnDirections.size() > 1) {
                return new NavigationResult(T_FORK);
            } else if (turnDirections.size() == 0) {
                return new NavigationResult(detectTurnError(previous, current));
            } else /* if (turnDirections.size() == 1) */ {
                return new NavigationResult(turnDirections.iterator().next());
            }
        }
    }

    /**
     * We should keep direction if current character is uppercase letter, and we can keep direction (i.e. next
     * position is same direction is traversable)
     *
     * @param previous Previous map location
     * @param current  Current map location
     * @return True if we should keep direction, false otherwise
     */
    private boolean shouldKeepDirection(MapLocation previous, MapLocation current) {
        return Character.isUpperCase(current.getCharacter()) && map.canKeepDirection(previous, current);
    }

    private boolean isPathBrokenTurn(MapLocation previous, MapLocation current) {
        return nowhereToGo(previous, current) && !visitedAllLocations();
    }

    private boolean isDeadEndTurn(MapLocation previous, MapLocation current) {
        return nowhereToGo(previous, current) && visitedAllLocations();
    }

    private boolean nowhereToGo(MapLocation previous, MapLocation current) {
        return !map.canKeepDirection(previous, current);
    }

    private boolean visitedAllLocations() {
        return map.countTraversableLocations() == path.getLocations().size();
    }

    /**
     * Fake turn is when we are at TURN character but have nowhere to turn and can keep current direction.
     *
     * @param previous Previous map location
     * @param current  Current map location
     * @return True if current position is fake turn, false otherwise
     */
    private boolean isFakeTurn(MapLocation previous, MapLocation current) {
        return map.canKeepDirection(previous, current);
    }

    private Set<Direction> findTurnDirections(MapLocation current, Direction previous) {
        return getAllTurnDirections(previous).stream()
                                             .filter(d -> map.canMakeStep(d, current))
                                             .collect(Collectors.toSet());
    }

    private boolean canMakeTurnAt(MapLocation location) {
        char locationChar = location.getCharacter();
        return locationChar == Characters.PATH_TURN || isUpperCase(locationChar);
    }

    private String detectTurnError(MapLocation previous, MapLocation current) {
        if (isFakeTurn(previous, current)) {
            return FAKE_TURN;
        } else if (isDeadEndTurn(previous, current)) {
            return NO_END;
        } else if (isPathBrokenTurn(previous, current)) {
            return BROKEN_PATH;
        }
        return UNKNOWN_ERROR;
    }

}
