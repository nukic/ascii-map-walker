package org.nukic.mapwalker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import org.junit.Assert;
import org.junit.Test;
import org.nukic.mapwalker.checker.MainChecker;
import org.nukic.mapwalker.infra.ResourceMaps;
import org.nukic.mapwalker.infra.LogMessages;
import org.nukic.mapwalker.reader.ResourceFileMapReader;

public class AdvancedTests {

    /**
     * Start-after-end, on the path
     */
    @Test
    public void testWalkingMap14() {
        var map = new ResourceFileMapReader(ResourceMaps.get(14)).read();
        var result = new MainChecker().check(map);
        Assert.assertEquals(LogMessages.MULTIPLE_STARTS_ERROR, result.getErrorMessage());
    }

    /**
     * No-end after end, ending with uppercase letter
     */
    @Test
    public void testWalkingMap15() {
        var map = new ResourceFileMapReader(ResourceMaps.get(15)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.isValid());
    }

    /**
     * No-end after end, ending with dash
     */
    @Test
    public void testWalkingMap16() {
        var map = new ResourceFileMapReader(ResourceMaps.get(16)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.isValid());
    }

    /**
     * Path broken after end, ending with dash
     */
    @Test
    public void testWalkingMap17() {
        var map = new ResourceFileMapReader(ResourceMaps.get(17)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.isValid());
    }

    /**
     * Path broken after end, ending with uppercase letter
     */
    @Test
    public void testWalkingMap18() {
        var map = new ResourceFileMapReader(ResourceMaps.get(18)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.isValid());
    }

    /**
     * Path broken after end, ending with uppercase letter and remaining part in different direction
     */
    @Test
    public void testWalkingMap19() {
        var map = new ResourceFileMapReader(ResourceMaps.get(19)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.isValid());
    }

    /**
     * Multiple ends, after first end
     */
    @Test
    public void testWalkingMap20() {
        var map = new ResourceFileMapReader(ResourceMaps.get(20)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.getErrorMessage().equals(LogMessages.MULTIPLE_ENDS_ERROR));
    }

    /**
     * Multiple starts, after first end
     */
    @Test
    public void testWalkingMap21() {
        var map = new ResourceFileMapReader(ResourceMaps.get(21)).read();
        var result = new MainChecker().check(map);
        Assert.assertEquals(LogMessages.MULTIPLE_STARTS_ERROR, result.getErrorMessage());
    }

    /**
     * Unreachable connected branch
     */
    @Test
    public void testWalkingMap22() {
        var map = new ResourceFileMapReader(ResourceMaps.get(22)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.isValid());
    }

    /**
     * Invalid characters in unreachable connected branch
     */
    @Test
    public void testWalkingMap23() {
        var map = new ResourceFileMapReader(ResourceMaps.get(23)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.getErrorMessage().equals(LogMessages.INVALID_CHARACTERS));
    }

}
