package org.nukic.mapwalker.checker;

import static org.junit.Assert.assertEquals;
import static org.nukic.mapwalker.infra.LogMessages.NO_END;


import org.junit.Test;
import org.nukic.mapwalker.reader.StringMapReader;

public class EndCheckerTest {

    @Test
    public void testPassingTest() {
        var map = new StringMapReader("@-B--A-x").read();
        assertEquals(true, new EndChecker().check(map).isValid());
    }

    @Test
    public void testPassingWithUnreachableMultipleEndsTest() {
        var map = new StringMapReader("@-B--A-x  -x").read();
        assertEquals(true, new EndChecker().check(map).isValid());
    }

    @Test
    public void testPassingWithReachableMultipleEndsTest() {
        var map = new StringMapReader("@-B--A-x-x").read();
        assertEquals(true, new EndChecker().check(map).isValid());
    }

    @Test
    public void testFailing() {
        var map = new StringMapReader("@-B--A-").read();
        assertEquals(NO_END, new EndChecker().check(map).getErrorMessage());
    }

}
