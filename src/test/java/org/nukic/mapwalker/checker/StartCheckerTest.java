package org.nukic.mapwalker.checker;

import static org.junit.Assert.assertEquals;
import static org.nukic.mapwalker.infra.LogMessages.NO_START_ERROR;


import org.junit.Test;
import org.nukic.mapwalker.reader.StringMapReader;

public class StartCheckerTest {

    @Test
    public void testSimplePassing() {
        var map = new StringMapReader("@-B--A-x").read();
        assertEquals(true, new StartChecker().check(map).isValid());
    }

    @Test
    public void testPassingWithUnreachableMultipleEndsTest() {
        var map = new StringMapReader("@-B--A-x  -@").read();
        assertEquals(true, new StartChecker().check(map).isValid());
    }

    @Test
    public void testPassingWithReachableMultipleEndsTest() {
        var map = new StringMapReader("@-B--A-x-@").read();
        assertEquals(true, new StartChecker().check(map).isValid());
    }

    @Test
    public void testFailing() {
        var map = new StringMapReader("-B--A-x").read();
        assertEquals(NO_START_ERROR, new StartChecker().check(map).getErrorMessage());
    }

}
