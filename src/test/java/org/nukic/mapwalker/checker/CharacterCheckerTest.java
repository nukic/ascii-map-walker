package org.nukic.mapwalker.checker;

import static org.junit.Assert.assertEquals;
import static org.nukic.mapwalker.infra.LogMessages.INVALID_CHARACTERS;


import org.junit.Test;
import org.nukic.mapwalker.reader.StringMapReader;

public class CharacterCheckerTest {

    @Test
    public void testSimplePassing() {
        var map = new StringMapReader("@-B--A-x").read();
        assertEquals(true, new CharacterChecker().check(map).isValid());
    }

    @Test
    public void testFailing() {
        var map = new StringMapReader("@-B--Ž-x").read();
        assertEquals(INVALID_CHARACTERS, new CharacterChecker().check(map).getErrorMessage());
    }

}
