package org.nukic.mapwalker.checker;

import static org.junit.Assert.assertEquals;


import org.junit.Assert;
import org.junit.Test;
import org.nukic.mapwalker.infra.LogMessages;
import org.nukic.mapwalker.reader.StringMapReader;

public class MainCheckerTest {

    /**
     * From walking checker
     */
    @Test
    public void testPassingWithUnreachableMultipleStarts() {
        var map = new StringMapReader("@-B--A-x  -@").read();
        assertEquals(true, new MainChecker().check(map).isValid());
    }

    @Test
    public void testFailingWithReachableMultipleStarts() {
        var map = new StringMapReader("@-B--A-x-@").read();
        Assert.assertEquals(LogMessages.MULTIPLE_STARTS_ERROR, new MainChecker().check(map).getErrorMessage());
    }

    @Test
    public void testPassingWithUnreachableMultipleEnds() {
        var map = new StringMapReader("@-B--A-x  -x").read();
        assertEquals(true, new MainChecker().check(map).isValid());
    }

    @Test
    public void testFailingWithReachableMultipleEnds() {
        var map = new StringMapReader("@-B--A-x-x").read();
        Assert.assertEquals(LogMessages.MULTIPLE_ENDS_ERROR, new MainChecker().check(map).getErrorMessage());
    }

    @Test
    public void testFailingOnTFork() {
        // @formatter:off
        var mapContent = "x--B\n" +
                         "   |\n" +
                         "@--+\n" +
                         "   |\n" +
                         "x--C\n";
        // @formatter:on
        var map = new StringMapReader(mapContent).read();
        Assert.assertEquals(LogMessages.T_FORK, new MainChecker().check(map).getErrorMessage());
    }

    @Test
    public void testPassingOnTurnUnreachableBranch() {
        // @formatter:off
        var mapContent = "x--B\n" +
                         "   |\n" +
                         "@--+--D\n";
        // @formatter:on
        var map = new StringMapReader(mapContent).read();
        assertEquals(true, new MainChecker().check(map).isValid());
    }

    @Test
    public void testFailingOnBrokenPath() {
        var map = new StringMapReader("@-B--A-   -x").read();
        Assert.assertEquals(LogMessages.BROKEN_PATH, new MainChecker().check(map).getErrorMessage());
    }

    @Test
    public void testFailingOnMultipleStartingPaths() {
        var map = new StringMapReader("A--@-B-x").read();
        Assert.assertEquals(LogMessages.MULTIPLE_STARTING_PATHS, new MainChecker().check(map).getErrorMessage());
    }

    /**
     * From Character checker
     */
    @Test
    public void testFailingOnInvalidCharacters() {
        var map = new StringMapReader("@-B--Ž-x").read();
        Assert.assertEquals(LogMessages.INVALID_CHARACTERS, new MainChecker().check(map).getErrorMessage());
    }

}
