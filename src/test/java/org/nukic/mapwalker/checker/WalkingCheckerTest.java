package org.nukic.mapwalker.checker;

import static org.junit.Assert.assertEquals;
import static org.nukic.mapwalker.infra.LogMessages.BROKEN_PATH;
import static org.nukic.mapwalker.infra.LogMessages.MULTIPLE_ENDS_ERROR;
import static org.nukic.mapwalker.infra.LogMessages.MULTIPLE_STARTING_PATHS;
import static org.nukic.mapwalker.infra.LogMessages.MULTIPLE_STARTS_ERROR;
import static org.nukic.mapwalker.infra.LogMessages.T_FORK;


import org.junit.Test;
import org.nukic.mapwalker.reader.StringMapReader;

public class WalkingCheckerTest {

    @Test
    public void testPassingWithUnreachableMultipleStarts() {
        var map = new StringMapReader("@-B--A-x  -@").read();
        assertEquals(true, new WalkingChecker().check(map).isValid());
    }

    @Test
    public void testFailingWithReachableMultipleStarts() {
        var map = new StringMapReader("@-B--A-x-@").read();
        assertEquals(MULTIPLE_STARTS_ERROR, new WalkingChecker().check(map).getErrorMessage());
    }

    @Test
    public void testPassingWithUnreachableMultipleEnds() {
        var map = new StringMapReader("@-B--A-x  -x").read();
        assertEquals(true, new WalkingChecker().check(map).isValid());
    }

    @Test
    public void testFailingWithReachableMultipleEnds() {
        var map = new StringMapReader("@-B--A-x-x").read();
        assertEquals(MULTIPLE_ENDS_ERROR, new WalkingChecker().check(map).getErrorMessage());
    }

    @Test
    public void testFailingOnTFork() {
        // @formatter:off
        var mapContent = "x--B\n" +
                         "   |\n" +
                         "@--+\n" +
                         "   |\n" +
                         "x--C\n";
        // @formatter:on
        var map = new StringMapReader(mapContent).read();
        assertEquals(T_FORK, new WalkingChecker().check(map).getErrorMessage());
    }

    @Test
    public void testPassingOnTurnUnreachableBranch() {
        // @formatter:off
        var mapContent = "x--B\n" +
                         "   |\n" +
                         "@--+--D\n";
        // @formatter:on
        var map = new StringMapReader(mapContent).read();
        assertEquals(true, new WalkingChecker().check(map).isValid());
    }

    @Test
    public void testFailingOnBrokenPath() {
        var map = new StringMapReader("@-B--A-   -x").read();
        assertEquals(BROKEN_PATH, new WalkingChecker().check(map).getErrorMessage());
    }

    @Test
    public void testFailingOnMultipleStartingPaths() {
        var map = new StringMapReader("A--@-B-x").read();
        assertEquals(MULTIPLE_STARTING_PATHS, new WalkingChecker().check(map).getErrorMessage());
    }

}
