package org.nukic.mapwalker.map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


import java.util.List;
import org.junit.Test;

public class CharactersTest {

    @Test
    public void testFailingOnInvalidMapCharacter() {
        assertFalse(Characters.isValidMapCharacter('Ž'));
    }

    @Test
    public void testFailingOnInvalidPathCharacter() {
        assertFalse(Characters.isValidPathCharacter('Ž'));
    }

    @Test
    public void testPassingOnNonLetterMapCharacters() {
        var chars = List.of(Characters.WHITESPACE, Characters.PATH_START, Characters.PATH_END, Characters.PATH_TURN, '-', '|');
        assertTrue(chars.stream().allMatch(c -> Characters.isValidMapCharacter(c)));
    }

    @Test
    public void testPassingOnUppercaseLetterMapCharacter() {
        var chars = List.of('A', 'B', 'C', 'D', 'E', 'G', 'H', 'I', 'L', 'N', 'O', 'S');
        assertTrue(chars.stream().allMatch(c -> Characters.isValidMapCharacter(c)));
    }

    @Test
    public void testFailingOnLowercaseLetterMapCharacter() {
        var chars = List.of('a', 'b', 'c', 'd', 'e', 'g', 'h', 'i', 'l', 'n', 'o', 's');
        assertTrue(chars.stream().allMatch(c -> !Characters.isValidMapCharacter(c)));
    }

    @Test
    public void testFailingOnSpaceAsPathCharacter() {
        assertFalse(Characters.isValidPathCharacter(Characters.WHITESPACE));

    }

}
