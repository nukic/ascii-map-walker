package org.nukic.mapwalker.map;

import static org.junit.Assert.assertEquals;


import java.util.List;
import org.junit.Test;
import org.nukic.mapwalker.walker.SunshineWalker;
import org.nukic.mapwalker.reader.StringMapReader;

public class PathTest {

    @Test
    public void testPathEquals() {
        var path1 = new Path();
        path1.save(new MapLocation(0, 0, Characters.PATH_START));
        path1.save(new MapLocation(0, 1, '-'));
        path1.save(new MapLocation(0, 2, Characters.PATH_END));
        var path2 = new Path();
        path2.save(new MapLocation(0, 0, Characters.PATH_START));
        path2.save(new MapLocation(0, 1, '-'));
        path2.save(new MapLocation(0, 2, Characters.PATH_END));
        assertEquals(path1, path2);
    }

    @Test
    public void testPathSavingLocation() {
        var path = new Path();
        path.save(new MapLocation(0, 0, Characters.PATH_START));
        path.save(new MapLocation(0, 1, '-'));
        path.save(new MapLocation(0, 2, 'A'));
        path.save(new MapLocation(0, 3, '-'));
        path.save(new MapLocation(0, 4, 'B'));
        path.save(new MapLocation(0, 5, Characters.PATH_END));
        assertEquals("@-A-Bx", path.getPathAsString());
    }

    @Test
    public void testPathLetterCollection() {
        var path = new Path();
        path.save(new MapLocation(0, 0, Characters.PATH_START));
        path.save(new MapLocation(0, 1, '-'));
        path.save(new MapLocation(0, 2, 'A'));
        path.save(new MapLocation(0, 3, '-'));
        path.save(new MapLocation(0, 4, 'B'));
        path.save(new MapLocation(0, 5, Characters.PATH_END));
        assertEquals("AB", path.getCollectedLettersAsString());
    }

    @Test
    public void testPathCounting() {
        var path = new Path();
        path.save(new MapLocation(0, 0, Characters.PATH_START));
        path.save(new MapLocation(0, 1, '-'));
        path.save(new MapLocation(0, 2, 'A'));
        path.save(new MapLocation(0, 3, '-'));
        path.save(new MapLocation(0, 4, 'B'));
        path.save(new MapLocation(0, 5, Characters.PATH_END));
        assertEquals(2, path.count('-'));
    }

    @Test
    public void testIncludingSameNonLetterLocationTwice() {
        var cm = new char[][] { { ' ', 'x', ' ' }, { '@', '-', '+' }, { ' ', '+', '+' } };
        var map = new AsciiMap(cm);
        var path = new SunshineWalker().walk(map);
        assertEquals(2, path.count('-'));
    }

    @Test
    public void testNotIncludingLetterFromSameLocationTwice() {
        var cm = new char[][] { { ' ', 'x', ' ' }, { '@', 'A', '+' }, { ' ', '+', '+' } };
        var map = new AsciiMap(cm);
        var path = new SunshineWalker().walk(map);
        assertEquals(2, path.count('A'));
        assertEquals("A", path.getCollectedLettersAsString());
    }

    @Test
    public void testGetFirst() {
        var map = new StringMapReader("@AAA-BBBx-").read();
        var path = new SunshineWalker().walk(map);
        var dash = path.getFirstLocationWithChar('-');
        assertEquals(dash, new MapLocation(0, 4, '-'));
    }

    @Test
    public void testGetAllAfter() {
        var map = new StringMapReader("@A-Bx").read();
        var path = new SunshineWalker().walk(map);
        var dash = path.getFirstLocationWithChar('-');
        var bs = path.getAllAfter(dash);
        var expected = List.of(new MapLocation(0, 2, '-'), new MapLocation(0, 3, 'B'), new MapLocation(0, 4, Characters.PATH_END));
        assertEquals(expected, bs);
    }

    @Test
    public void testGetAllBefore() {
        var map = new StringMapReader("@A-Bx").read();
        var path = new SunshineWalker().walk(map);
        var dash = path.getFirstLocationWithChar('-');
        var as = path.getAllBefore(dash);
        assertEquals(List.of(new MapLocation(0, 0, Characters.PATH_START), new MapLocation(0, 1, 'A')), as);
    }

}
