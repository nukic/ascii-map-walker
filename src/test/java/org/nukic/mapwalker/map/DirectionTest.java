package org.nukic.mapwalker.map;

import static org.junit.Assert.assertEquals;
import static org.nukic.mapwalker.infra.LogMessages.DELTAS_MUST_NOT_BE_LARGER_THAN_1;
import static org.nukic.mapwalker.infra.LogMessages.DELTAS_MUST_NOT_BE_SAME;
import static org.nukic.mapwalker.map.Direction.DOWN;
import static org.nukic.mapwalker.map.Direction.LEFT;
import static org.nukic.mapwalker.map.Direction.RIGHT;
import static org.nukic.mapwalker.map.Direction.UP;
import static org.nukic.mapwalker.map.Direction.getAllTurnDirections;
import static org.nukic.mapwalker.map.Direction.getOpposite;


import java.util.Set;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.nukic.mapwalker.infra.MapWalkerException;

public class DirectionTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testUpOpposite() {
        assertEquals(UP, getOpposite(DOWN));

    }

    @Test
    public void testDownOpposite() {
        assertEquals(DOWN, getOpposite(UP));
    }

    @Test
    public void testRightOpposite() {
        assertEquals(RIGHT, getOpposite(LEFT));
    }

    @Test
    public void testLeftOpposite() {
        assertEquals(LEFT, getOpposite(RIGHT));
    }

    @Test
    public void testUpTurnDirections() {
        assertEquals(Set.of(RIGHT, LEFT), getAllTurnDirections(UP));
    }

    @Test
    public void testGetDownTurnDirections() {
        assertEquals(Set.of(RIGHT, LEFT), getAllTurnDirections(DOWN));
    }

    @Test
    public void testGetLeftTurnDirections() {
        assertEquals(Set.of(UP, DOWN), getAllTurnDirections(LEFT));
    }

    @Test
    public void testGetRightTurnDirections() {
        assertEquals(Set.of(UP, DOWN), getAllTurnDirections(RIGHT));
    }

    @Test
    public void testDownFromDeltas() {
        assertEquals(DOWN, Direction.fromDeltas(1, 0));
    }

    @Test
    public void testUpFromDeltas() {
        assertEquals(UP, Direction.fromDeltas(-1, 0));
    }

    @Test
    public void testRightFromDeltas() {
        assertEquals(RIGHT, Direction.fromDeltas(0, 1));
    }

    @Test
    public void testLeftFromDeltas() {
        assertEquals(LEFT, Direction.fromDeltas(0, -1));
    }

    @Test
    public void testDiagonalFromDeltas() {
        exceptionRule.expect(MapWalkerException.class);
        Direction.fromDeltas(1, 1);
        exceptionRule.expectMessage(DELTAS_MUST_NOT_BE_SAME);
    }

    @Test
    public void testDiagonalFromDeltasNotSame() {
        exceptionRule.expect(MapWalkerException.class);
        Direction.fromDeltas(1, -1);
        exceptionRule.expectMessage(DELTAS_MUST_NOT_BE_SAME);
    }

    @Test
    public void testMultistepFromDeltas() {
        exceptionRule.expect(MapWalkerException.class);
        Direction.fromDeltas(3, 0);
        exceptionRule.expectMessage(DELTAS_MUST_NOT_BE_LARGER_THAN_1);
    }

    @Test
    public void testFindingDownStepDirection() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var previous = map.getMapLocation(0, 0);
        var current = map.getMapLocation(1, 0);
        assertEquals(DOWN, Direction.findStepDirection(previous, current));
    }
    @Test
    public void testFindingUpStepDirection() {
        var cm = new char[][] { { '+', '-', 'x' }, { '@', ' ', ' ' } };
        var map = new AsciiMap(cm);
        var previous = map.getMapLocation(1, 0);
        var current = map.getMapLocation(0, 0);
        assertEquals(UP, Direction.findStepDirection(previous, current));
    }


    @Test
    public void testFindingRightStepDirection() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var previous = map.getMapLocation(1, 0);
        var current = map.getMapLocation(1, 1);
        assertEquals(RIGHT, Direction.findStepDirection(previous, current));
    }

    @Test
    public void testFindingLeftStepDirection() {
        var cm = new char[][] { { 'x', '-', '+' }, { ' ', ' ', '@' } };
        var map = new AsciiMap(cm);
        var previous = map.getMapLocation(0, 2);
        var current = map.getMapLocation(0, 1);
        assertEquals(LEFT, Direction.findStepDirection(previous, current));
    }


}
