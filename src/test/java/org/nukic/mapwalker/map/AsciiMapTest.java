package org.nukic.mapwalker.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.nukic.mapwalker.infra.LogMessages.NOT_ON_A_MAP;
import static org.nukic.mapwalker.map.Direction.DOWN;
import static org.nukic.mapwalker.map.Direction.LEFT;
import static org.nukic.mapwalker.map.Direction.RIGHT;
import static org.nukic.mapwalker.map.Direction.UP;


import java.util.List;
import java.util.Set;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.nukic.mapwalker.infra.MapWalkerException;
import org.nukic.mapwalker.infra.ResourceMaps;
import org.nukic.mapwalker.reader.ResourceFileMapReader;
import org.nukic.mapwalker.reader.StringMapReader;

public class AsciiMapTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testFindingStart() {
        var map = new StringMapReader("@-B--A-x").read();
        var found = map.findStart();
        var expected = new MapLocation(0, 0, Characters.PATH_START);
        assertEquals(expected, found);
    }

    @Test
    public void testFindingStartInTheMiddle() {
        var cm =
            new char[][] { { 'x', '-', 'B', '-', '-', 'A', '-', '+' }, { ' ', ' ', ' ', ' ', ' ', '@', '-', '+' } };
        var map = new AsciiMap(cm);
        var found = map.findStart();
        var expected = new MapLocation(1, 5, Characters.PATH_START);
        assertEquals(expected, found);
    }

    @Test
    public void testFindingLocationsWithCharacters() {
        var map = new StringMapReader("@-A-\nx-A+").read();
        var found = map.findLocationsWithChar('A');
        var expected = List.of(new MapLocation(0, 2, 'A'), new MapLocation(1, 2, 'A'));
        assertEquals(expected, found);
    }

    @Test
    public void testFindingTraversableLocations() {
        var map = new StringMapReader("@   \n+x  ").read();
        var found = map.findTraversableLocations();
        var expected = List.of(new MapLocation(0, 0, Characters.PATH_START), new MapLocation(1, 0, Characters.PATH_TURN),
                               new MapLocation(1, 1, Characters.PATH_END));
        assertEquals(expected, found);
    }

    @Test
    public void testFindingInvalidLocations() {
        var map = new StringMapReader("@  Ž\n+x Ž").read();
        var found = map.findInvalidLocations();
        var expected = List.of(new MapLocation(0, 3, 'Ž'), new MapLocation(1, 3, 'Ž'));
        assertEquals(expected, found);
    }

    @Test
    public void testCountingLocations() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var found = map.countTraversableLocations();
        assertEquals(4, found);
    }

    @Test
    public void testFindingLocations() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var found = map.findMapLocations(c -> c == ' ');
        var expected = List.of(new MapLocation(0, 1, ' '), new MapLocation(0, 2, ' '));
        assertEquals(expected, found);
    }

    @Test
    public void testGettingLocation() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var found = map.getMapLocation(0, 2);
        var expected = new MapLocation(0, 2, ' ');
        assertEquals(expected, found);
    }

    @Test
    public void testFailingGettingLocation() {
        exceptionRule.expect(MapWalkerException.class);
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        exceptionRule.expectMessage(String.format(NOT_ON_A_MAP, 100, 100, map));
        var found = map.getMapLocation(100, 100);
    }

    @Test
    public void testMakingStep() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var start = new MapLocation(0, 0, Characters.PATH_START);
        var found = map.makeStep(DOWN, start);
        var expected = new MapLocation(1, 0, Characters.PATH_TURN);
        assertEquals(expected, found);
    }

    @Test
    public void testMakingStepOutsideMap() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var start = new MapLocation(0, 0, Characters.PATH_START);
        var found = map.makeStep(LEFT, start);
        assertEquals(null, found);
    }

    @Test
    public void testMakingStepToUntraversableLocation() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var start = new MapLocation(0, 0, Characters.PATH_START);
        var found = map.makeStep(RIGHT, start);
        assertEquals(null, found);
    }

    @Test
    public void testIfCanKeepDirection() {
        var cm = new char[][] { { '@', '-', '-', '-', 'x' } };
        var map = new AsciiMap(cm);
        var previous = new MapLocation(0, 1, '-');
        var current = new MapLocation(0, 2, '-');
        assertTrue(map.canKeepDirection(previous, current));
    }

    @Test
    public void testIfCannotKeepDirection() {
        var cm = new char[][] { { '@', '-', '+', ' ', ' ' }, { 'x', '-', '+', ' ', ' ' } };
        var map = new AsciiMap(cm);
        var previous = new MapLocation(0, 1, '-');
        var current = new MapLocation(0, 2, '-');
        assertFalse(map.canKeepDirection(previous, current));
    }

    @Test
    public void testIfCanMakeStep() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var start = new MapLocation(0, 0, Characters.PATH_START);
        assertTrue(map.canMakeStep(DOWN, start));
    }

    @Test
    public void testIfCanMakeStepOutsideMap() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var start = new MapLocation(0, 0, Characters.PATH_START);
        assertFalse(map.canMakeStep(LEFT, start));
    }

    @Test
    public void testIfCanMakeStepToUntraversableLocation() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var start = new MapLocation(0, 0, Characters.PATH_START);
        assertFalse(map.canMakeStep(RIGHT, start));
    }

    @Test
    public void testGettingPossibleDirections() {
        var cm = new char[][] { { '@', ' ', ' ' }, { '+', '-', 'x' } };
        var map = new AsciiMap(cm);
        var current = map.getMapLocation(0, 0);
        var found = map.getPossibleDirections(current);
        var expected = Set.of(DOWN);
        assertEquals(expected, found);
    }

    @Test
    public void testGettingPossibleDirectionsComplex() {
        var map = new ResourceFileMapReader(ResourceMaps.MAP2).read();
        var current = map.getMapLocation(3, 4);
        var found = map.getPossibleDirections(current);
        var expected = Set.of(UP, DOWN, RIGHT, LEFT);
        assertEquals(expected, found);
    }

}
