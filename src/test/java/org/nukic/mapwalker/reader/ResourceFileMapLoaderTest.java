package org.nukic.mapwalker.reader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import org.junit.Test;
import org.nukic.mapwalker.map.AsciiMap;

public class ResourceFileMapLoaderTest {

    private String filePath = "maps/valid/map1.txt";
    private final String maps1AsString =
        "  @---A---+\n" + "          |\n" + "  x-B-+   C\n" + "      |   |\n" + "      +---+\n";

    @Test
    public void testReadingFile() {
        AsciiMap map = new ResourceFileMapReader(filePath).read();
        assertEquals(maps1AsString, map.toString());
    }

}
