package org.nukic.mapwalker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.nukic.mapwalker.infra.LogMessages.BROKEN_PATH;
import static org.nukic.mapwalker.infra.LogMessages.FAKE_TURN;
import static org.nukic.mapwalker.infra.LogMessages.MULTIPLE_ENDS_ERROR;
import static org.nukic.mapwalker.infra.LogMessages.MULTIPLE_STARTING_PATHS;
import static org.nukic.mapwalker.infra.LogMessages.MULTIPLE_STARTS_ERROR;
import static org.nukic.mapwalker.infra.LogMessages.NO_END;
import static org.nukic.mapwalker.infra.LogMessages.NO_START_ERROR;
import static org.nukic.mapwalker.infra.LogMessages.T_FORK;


import org.junit.Test;
import org.nukic.mapwalker.checker.MainChecker;
import org.nukic.mapwalker.infra.ResourceMaps;
import org.nukic.mapwalker.reader.ResourceFileMapReader;
import org.nukic.mapwalker.walker.SunshineWalker;

public class AcceptanceTests {

    @Test
    public void testWalkingMap1() {
        var map = new ResourceFileMapReader(ResourceMaps.get(1)).read();
        var actual = new SunshineWalker().walk(map);
        assertEquals("@---A---+|C|+---+|+-B-x", actual.getPathAsString());
        assertEquals("ACB", actual.getCollectedLettersAsString());
    }

    @Test
    public void testWalkingMap2() {
        var map = new ResourceFileMapReader(ResourceMaps.get(2)).read();
        var actual = new SunshineWalker().walk(map);
        assertEquals("@|A+---B--+|+--C-+|-||+---D--+|x", actual.getPathAsString());
        assertEquals("ABCD", actual.getCollectedLettersAsString());
    }

    @Test
    public void testWalkingMap3() {
        var map = new ResourceFileMapReader(ResourceMaps.get(3)).read();
        var actual = new SunshineWalker().walk(map);
        assertEquals("@---A---+|||C---+|+-B-x", actual.getPathAsString());
        assertEquals("ACB", actual.getCollectedLettersAsString());
    }

    @Test
    public void testWalkingMap4() {
        var map = new ResourceFileMapReader(ResourceMaps.get(4)).read();
        var actual = new SunshineWalker().walk(map);
        assertEquals("@-G-O-+|+-+|O||+-O-N-+|I|+-+|+-I-+|ES|x", actual.getPathAsString());
        assertEquals("GOONIES", actual.getCollectedLettersAsString());
    }

    @Test
    public void testWalkingMap5() {
        var map = new ResourceFileMapReader(ResourceMaps.get(5)).read();
        var actual = new SunshineWalker().walk(map);
        assertEquals("@B+++B|+-L-+A+++A-+Hx", actual.getPathAsString());
        assertEquals("BLAH", actual.getCollectedLettersAsString());
    }

    @Test
    public void testValidatingMap6() {
        var map = new ResourceFileMapReader(ResourceMaps.get(6)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.getErrorMessage().equals(NO_START_ERROR));
    }

    @Test
    public void testValidatingMap7() {
        var map = new ResourceFileMapReader(ResourceMaps.get(7)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.getErrorMessage().startsWith(NO_END));
    }

    @Test
    public void testValidatingMap8() {
        var map = new ResourceFileMapReader(ResourceMaps.get(8)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.getErrorMessage().equals(MULTIPLE_STARTS_ERROR));
    }

    @Test
    public void testValidatingMap9() {
        var map = new ResourceFileMapReader(ResourceMaps.get(9)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.getErrorMessage().equals(MULTIPLE_ENDS_ERROR));
    }

    @Test
    public void testWalkingMap10() {
        var map = new ResourceFileMapReader(ResourceMaps.get(10)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.getErrorMessage().startsWith(T_FORK));
    }

    @Test
    public void testWalkingMap11() {
        var map = new ResourceFileMapReader(ResourceMaps.get(11)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.getErrorMessage().startsWith(BROKEN_PATH));
    }

    @Test
    public void testWalkingMap12() {
        var map = new ResourceFileMapReader(ResourceMaps.get(12)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.getErrorMessage().equals(MULTIPLE_STARTING_PATHS));
    }

    @Test
    public void testWalkingMap13() {
        var map = new ResourceFileMapReader(ResourceMaps.get(13)).read();
        var result = new MainChecker().check(map);
        assertTrue(result.getErrorMessage().startsWith(FAKE_TURN));
    }
}
