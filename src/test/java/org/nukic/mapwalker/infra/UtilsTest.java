package org.nukic.mapwalker.infra;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import java.nio.file.Paths;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class UtilsTest {

    private static final String VALID_MAP_PATH_REGEX = ".*/map[1-5].txt";

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testToCharMatrix() {
        var str = "AAA\nBBB\nCCC\n";
        var expected = new char[][] { { 'A', 'A', 'A' }, { 'B', 'B', 'B' }, { 'C', 'C', 'C' } };
        var obtained = Utils.toCharMatrix(str);
        assertArrayEquals(expected, obtained);
    }

    @Test
    public void testToString() {
        var cm = new char[][] { { 'A', 'A', 'A' }, { 'B', 'B', 'B' }, { 'C', 'C', 'C' } };
        var expected = "AAA\nBBB\nCCC\n";
        var obtained = Utils.toString(cm);
        assertEquals(expected, obtained);
    }

    @Test
    public void testContainedIn() {
        var array = new Character[] { 'A', 'A', 'A' };
        assertTrue(Utils.containedIn(array, 'A'));
    }

    @Test
    public void testReadingResources() {
        var content = Utils.readResourceRelativePathFile(ResourceMaps.MAP12);
        assertEquals("  -B-@-A-x\n", content);
    }

    @Test
    public void testFailingToReadResources() {
        exceptionRule.expect(MapWalkerException.class);
        var path = "not_existing";
        exceptionRule.expectMessage(String.format(LogMessages.CANNOT_READ_PATH, path));
        Utils.readResourceRelativePathFile(path);
    }

    @Test
    public void testReadingAbsoluteFilePath() {
        var absolutePath = Paths.get(Utils.class.getClassLoader().getResource(ResourceMaps.MAP12).getPath()).toAbsolutePath().toString();
        var content = Utils.readAbsolutePathFile(absolutePath);
        assertEquals("  -B-@-A-x\n", content);
    }

    @Test
    public void testFailingToReadAbsoluteFile() {
        exceptionRule.expect(MapWalkerException.class);
        var absolutePath = Paths.get(Utils.class.getClassLoader().getResource(ResourceMaps.MAP12).getPath()).toAbsolutePath().toString() + "not_exists";
        exceptionRule.expectMessage(String.format(LogMessages.CANNOT_READ_PATH, absolutePath));
        Utils.readAbsolutePathFile(absolutePath);
    }

    @Test
    public void testReadingResourceDirPaths() {
        var resourceDir = "maps/valid";
        var files = Utils.getResourceDirFilePaths(resourceDir);
        assertTrue(files.size()==5 && files.stream().allMatch(f -> f.matches(VALID_MAP_PATH_REGEX)));
    }

    @Test
    public void testFailingToReadResourceDirPaths() {
        exceptionRule.expect(MapWalkerException.class);
        var resourceDir = "maps/not_existing";
        exceptionRule.expectMessage(String.format(LogMessages.CANNOT_READ_PATH, resourceDir));
        Utils.getResourceDirFilePaths(resourceDir);
    }

    @Test
    public void testFailingToReadResourceDirPathsWhenPathIsFile() {
        exceptionRule.expect(MapWalkerException.class);
        var resourceDir = ResourceMaps.MAP1;
        exceptionRule.expectMessage(String.format(LogMessages.INVALID_DIR_PATH, resourceDir));
        Utils.getResourceDirFilePaths(resourceDir);
    }

}
