# Ascii Map Walker

Ascii Map Walker is simple java CLI application that implements algorithm for walking ASCII maps as described [here](https://github.com/softwaresauna/code-challenge) 

## Usage
To build the jar package just run:
```bash
mvn clean package
```
or use your favorite IDE to do the same.



To run the application with one of the predefined maps:
```java
java -cp AsciiMapWalker-<VERSION>.jar org.nukic.mapwalker.App <MAP_NUMBER>
```
There are 23 predefined maps, so you can use numbers from 1 to 23. 



If you want to test the app with your own maps:
```java
java -cp AsciiMapWalker-<VERSION>.jar org.nukic.mapwalker.App <ABSOLUTE_MAP_PATH>
```

# Requirements
Application is tested with Java 16, you should be able to run the app with any java version >= 1.8.
To build the jar, you should have maven installed. Check [this page](https://maven.apache.org/install.html) if you need assistance.


## Author
[Nenad Ukić](https://www.linkedin.com/in/nenad-ukic/)